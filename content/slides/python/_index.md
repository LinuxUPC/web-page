+++
title = "Python"
description = "Introduction to python"
outputs = ["Reveal"]
[reveal_hugo]
theme = "night"
+++

# Python
_Introduction to python_

---
# Syntax
## conditionals
if-else
```python
if condition:
  pass
else:
  pass
```
if-elif-else

```python
if condition:
  pass
elif condition:
  pass
else:
  pass
```
---


# Syntax
## loops
for loop
```python
for value in iterable:
  pass
```
while loop
```python
while condition:
  pass
```

---

# types

All types inherit from `object`

- `list`: `[-1, 3.14, "Hello World!", False]`
- `tuple`: `(-1, 3.14, "Hello World!", False)`
- `dict`: `{"Hi": "Hola", "Bye": "Adiós"}`
- `iter`: `iter(partial(open(file).read, chunck_size), "")`
- `int`: `21`
- `float`: `2.019e3`
- `bool`: `True`
- `str`: `"Hello World!`

- `function`: `def foo(): pass`, `lambda x: x**2`

---

# types categories

- Iterables: `list`, `tuple`,  `dict`, `iter`

Iterable types are types that can be looped over

- Mutable: `list`, `dict`

Objects of mutable type can change over time but these are unhashable meaning
that they can't be used as a key of a dictionary.
```
l = [1,2,3]
id_before = id(l)
l.append(4)
id_after = id(l)
assert(id_before == id_after)
```

---

# types categories

- Immutable: `tuple`,  `str`, `int`, `float`
Objects of immutable type can't change
```
x = 1
id_before = id(x)
x += 1
id_after = id(x)
assert(id_before != id_after)
```
