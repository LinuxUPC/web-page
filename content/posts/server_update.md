---
title: "Actualitzacio del servidor"
date: 2020-01-29T10:53:17+02:00
---

Avui dia 29 de Gener del 2020, a LinuxUPC hem actualitzat el servidor. Ara mateix nomes serveix la pagina web de l'associacio, nogensmenys tenim pensat afegir-hi mes serveis durant els propers dies.
El servidor utilitza la ultima versio de Debian (10 buster), i consta de 8 nuclis de processement, doblant quatre vegades els nuclis del servidor anterior. A mes a mes hem passat de mig gigabyte de ram a a 16.
