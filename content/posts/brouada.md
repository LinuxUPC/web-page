---
title: "La brouada ha estat tot un exit"
date: 2020-01-14T16:48:17+02:00
author: "Guillem Ramírez Miranda (PDL)"
---
Com cada any, a LinuxUPC vam fer el passat 20 de desembre una brouada i cantada de Nadales. 

![photo1][https://linuxupc.upc.edu/posts/foto1-brouada.jpeg]
Varem repartir mes de *20 litros de brou* a les estudiants del campus! També vam crear cultura del software lliure al voltant de la comunitat universitaria mitjançant preguntes sobre el mateix.

Durant l'event, vam tenir la participació de personalitats importants al campus, com pot ser, en Rafel Albert Bross i tots els Guillem Ramirez del campus.

Com a conclusió podem dir que va ser tot un éxit i vam complir amb tots els objectius que tenia la activitat, a més, vam crear caliu i vam fer comunitat.

Ens veiem el proper any!
