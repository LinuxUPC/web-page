---
title: "Taller d'introducció al Terminal de Linux"
date: 2019-10-16
publishdate: 2019-09-26
---

Acabes d'entrar al terminal i estàs confós? T'agradaria aprendre les meravelles del terminal de Linux?
Vine al curs d'"Introducció al Terminal" de LinuxUPC! I seràs un as del bash scripting.

## Info

- Dia: `13 de novembre 2019`
- Lloc: `UPC Campus Nord aula TBD`

**Apuntat aquí:**
{{< rawhtml >}}
<div class="typeform-widget" data-url="https://linuxupc.typeform.com/to/Dvxc9Z" data-hide-headers=true data-hide-footer=true style="width: 100%; height: 500px;"></div> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script> <div style="font-family: Sans-Serif;font-size: 12px;color: #999;opacity: 0.5; padding-top: 5px;"> powered by <a href="https://admin.typeform.com/signup?utm_campaign=Dvxc9Z&utm_source=typeform.com-14687162-Free&utm_medium=typeform&utm_content=typeform-embedded-poweredbytypeform&utm_term=EN" style="color: #999" target="_blank">Typeform</a> </div>
{{< /rawhtml >}} 
