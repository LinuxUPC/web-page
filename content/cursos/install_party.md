---
title: "Linux Install Party"
date: 2019-09-25
publishdate: 2019-09-18
---

T'apassiona el món del **software lliure**, t'agradaria formar part d'aquest però
segueixes amb Windows. Doncs has vingut all lloc correcte, el dia 25/09
estàs convidat a la **Install Party** on ensenyarem a instal·lar **GNU/Linux** juntament amb
tots els coneixements introductoris necessaris per fer-lo servir.

## Info

- Dia: `25 de setembre 2019`
- Lloc: `A5102`

**Apuntat aquí:**
{{< rawhtml >}}
<div class="typeform-widget" data-url="https://linuxupc.typeform.com/to/ZK7gXf" style="width: 100%; height: 500px;"></div> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script> <div style="font-family: Sans-Serif;font-size: 12px;color: #999;opacity: 0.5; padding-top: 5px;"> powered by <a href="https://admin.typeform.com/signup?utm_campaign=ZK7gXf&utm_source=typeform.com-14687162-Free&utm_medium=typeform&utm_content=typeform-embedded-poweredbytypeform&utm_term=EN" style="color: #999" target="_blank">Typeform</a> </div>
{{< /rawhtml >}} 
