---
title: "El noi de la mare"
date: 2019-11-05
---


Què li darem en el Noi de la Mare?<br>
Què li darem que li sàpiga bo?<br>
Panses i figues i nous i olives,<br>
panses i figues i mel i mató.<br>
<br>
Què li darem al fillet de Maria?<br>
Què li darem al formós infantó?<br>
Li darem panses amb unes balances,<br>
li darem figues amb un paneró.<br>
<br>
Tam, pa-tam-tam, que les figues són verdes,<br>
Tam, pa-tam-tam, que ja maduraran.<br>
Si no maduren el dia de Pasqua,<br>
maduraran en el dia de Rams.<br>
<br>
Una cançó jo també cantaria,<br>
una cançó ben bonica d'amor;<br>
i que n'és treta d'una donzelleta,<br>
que n'és la Verge, Mare del Senyor. (1)<br>
<br>
No ploris, no manyaguet de la mare,<br>
no ploris, no, ai alè del meu cor!<br>
Cançó és aquesta que al Noi de la Mare,<br>
cançó és aquesta que li agrada molt.<br>
<br>
Tam, pa-tam-tam, que les figues són verdes,<br>
Tam, pa-tam-tam, que ja maduraran.<br>
Si no maduren el dia de Pasqua,<br>
maduraran en el dia de Rams.<br>
<br>
Àngels del cel són els que l’en bressolen,<br>
àngels del cel que li fan venir son,<br>
mentre li canten cançons d’alegria,<br>
cants de la glòria que no són del món. (2)<br>
<br>