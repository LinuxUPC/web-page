---
title: "Quan somrius"
date: 2019-11-05
---

Ara que la nit s'ha fet més llarga<br>
Ara que les fulles ballen danses al racó<br>
Ara que els carrers estan de festa<br>
Avui que la fed du tants records<br>
<br>
Ara que sobren les paraules<br>
Ara que el vent bufa tant fort<br>
Avui que no em fa falta veure't, ni tan sols parlar<br>
Per saber que estàs al meu costat<br>
<br>
És Nadal al meu cor<br>
Quan somrius content de veure'm<br>
Quan la nit és fa més freda<br>
Quan t'abraces al meu cos<br>
<br>
I les llums de colors<br>
M'il·luminen nit i dia<br>
Les encens amb el somriure<br>
Quan em parles amb el cor<br>
<br>
És el buit que deixes quan t'aixeques<br>
És el buit que és fa a casa quan no hi ha ningú<br>
Són petits detalls tot el que hem queda<br>
Com queda al jersei un cabell larg<br>
<br>
Vas dir que mai més tornaries<br>
El temps pacient ha anat passant<br>
Qui havia de dir que avui estaries esperant<br>
Que ens trobéssim junts al teu costat<br>
<br>
És Nadal al teu cor<br>
Quant somric content de veure't<br>
Quan la nit es fa més neta<br>
Quan m'abraço al teu cos<br>
<br>
I les llums de colors<br>
M'il·luminen nit i dia<br>
Les encén el teu somriure<br>
Quan et parlo amb el cor<br>
