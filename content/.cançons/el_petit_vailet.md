---
title: "El petit vailet"
date: 2019-11-04
---

Jo sóc el petit vailet,<br>
cansadet de molt camí,<br>
vinc amb el meu gaiatet<br>
per veure Jesús diví.<br>
<br>
Xerrampim, xerrampim,<br>
xerrampia,<br>
Xerrampim, xerrampim,<br>
xerrampo,<br>
xerrampim que Josep i Maria<br>
tenen un petit minyó.<br>
<br>
He portat la carmanyola<br>
tota plena de vi blanc,<br>
ametlles, mel i formatge<br>
per Jesús, el diví infant.<br>
<br>
Xerrampim, xerrampim,<br>
xerrampia,<br>
Xerrampim, xerrampim,<br>
xerrampo,<br>
xerrampim que Josep i Maria<br>
tenen un petit minyó.<br>
<br>
Si portés jo més recapte,<br>
també seria per a Vós,<br>
heus aquí el gaiat que porto,<br>
que és de cirerer d'arboç.<br>
<br>
Xerrampim, xerrampim,<br>
xerrampia,<br>
Xerrampim, xerrampim,<br>
xerrampo,<br>
xerrampim que Josep i Maria<br>
tenen un petit minyó<br>
