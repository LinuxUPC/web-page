---
title: "El Decembre congelat"
date: 2019-11-04
---

El desembre congelat<br>
confús es retira.<br>
Abril, de flors coronat,<br>
tot el món admira.<br>
Quan en un jardí d’amor<br>
neix una divina flor,<br>
d’una ro, ro, ro,<br>
d’una sa, sa, sa,<br>
d’una ro, d’una sa,<br>
d’una rosa bella,<br>
fecunda i poncella.<br>
<br>
El primer pare causà<br>
la nit tenebrosa<br>
que a tot el món ofuscà<br>
la vista penosa;<br>
mes, en una mitjanit,<br>
brilla el sol que n’és eixit<br>
d’una be, be, be,<br>
d’una lla, lla, lla,<br>
d’una be, d’una lla,<br>
d’una bella aurora<br>
que el cel enamora.<br>
<br>
El mes de maig ha florit,<br>
sense ser encara,<br>
un lliri blanc tot polit<br>
de fragància rara,<br>
que per tot el món se sent,<br>
de Llevant fins a Ponent,<br>
tota sa, sa, sa,<br>
tota dul, dul, dul,<br>
tota sa, tota dul,<br>
tota sa dulçura<br>
i olor, amb ventura.<br>
<br>
Arribaren els tres reis<br>
amb molta alegria,<br>
adorant el rei del cel<br>
en una establia.<br>
Oferint-li tres presents,<br>
com són or, mirra i encens.<br>
A la Ma, Ma, Ma,<br>
a la re, re, re,<br>
a la Ma, a la re,<br>
a la Mare pia,<br>
la Verge Maria.<br>
<br>
Amb contentament i amor<br>
celebrem el dia,<br>
en què el diví Senyor<br>
neix amb alegria.<br>
Si no tenim més tresor<br>
oferim-li nostre cor,<br>
que és la gran, gran, gran<br>
que és la fi, fi, fi<br>
que és la gran, que és la fi,<br>
que és la gran finesa<br>
<br>
de nostra pobresa.<br>
