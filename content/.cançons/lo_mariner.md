---
title: "Lo Mariner"
date: 2019-10-17
---

A la voreta del mar<br>
n'hi ha una donzella (bis)<br>
que en brodava un mocador,<br>
la flor més bella. (bis)<br>

Com ne fou a mig brodar<br>
li manca seda. (bis)<br>
Gira els ulls envers la mar:<br>
veu una vela; (bis)<br>

veu venir un galió<br>
tot vora terra; (bis)<br>
ne veu venir un mariner<br>
que una nau mena. (bis)<br>

-Mariner, bon mariner,<br>
que en porteu seda?- (bis)<br>
-De quin color la voleu,<br>
blanca o vermella?- (bis)<br>

-Vermelleta la vull jo,<br>
que és millor seda. (bis)<br>
Vermelleta la vull jo,<br>
que és per fer gresca.- (bis)<br>

-Pugeu a dalt de la nau,<br>
triareu d'ella.- (bis)<br>
-Ai no!, no hi puc pujar,<br>
no tinc moneda. (bis)<br>

Lo meu pare té les claus<br>
de l'arquimesa.- (bis)<br>
-No quedeu per diners, no,<br>
prou fio d'ella.- (bis)<br>

La donzella entra a la nau,<br>
tria al seda. (bis)<br>
Mentre va mercadejant,<br>
la nau pren vela. (bis)<br>

Mariner es posa a cantar<br>
cançons novelles. (bis)<br>
Amb lo cant del mariner<br>
s'ha dormideta, (bis)<br>

i amb el soroll de la mar<br>
ella es desperta. (bis)<br>
Quan ella s'ha despertat<br>
ja no en veu terra: (bis)<br>

la nau és en alta mar,<br>
pel mar navega. (bis)<br>
-Mariner, bon mariner,<br>
torneu-ne en terra, (bis)<br>

que los aires de la mar<br>
me'n donen pena.- (bis)<br>
-Això sí que no ho faré,<br>
que estem en guerra. (bis)<br>

Set anys fa que vaig pel mar<br>
per vós donzella; (bis)<br>
cent llegües dins de la mar,<br>
lluny de la terra.- (bis)<br>

-De tres germanes que som,<br>
só la més bella: (bis)<br>
l'una porta vestit d'or,<br>
l'altra de seda (bis)<br>

i jo, pobreta de mi,<br>
de sargil negre. (bis)<br>
L'una és casada amb un duc,<br>
l'altra és princesa (bis)<br>
i jo, pobreta de mi,<br>
só marinera.- (bis)<br>

-Tot i ser marinera, vos,<br>
só la mes bella (bis)<br>
-Pero podem celebrar,<br>
que no só burgessa. (bis)<br>
